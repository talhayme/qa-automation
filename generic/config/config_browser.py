import logging
import os
from selenium.webdriver import DesiredCapabilities
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.microsoft import IEDriverManager

class Test_ConfigBrowser:
    def __init__(self):
        self.ENVIRONMENT = os.getenv("env")
        self.EXECUTION_TYPE = os.getenv("execution_type")
        self.BROWSER = os.getenv("browser")

    def firefox_capabilities(self):
        capabilities = DesiredCapabilities.FIREFOX
        return capabilities

    def chrome_capabilities(self):
        chrome_capabilities = DesiredCapabilities.CHROME
        return chrome_capabilities

    def chrome_headless_capabilities(self):
        options = webdriver.ChromeOptions()
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        capabilities = options.to_capabilities()
        return capabilities

    def set_capabilities(self):
        browser = self.BROWSER
        capabilities = None
        if browser == 'firefox':
            capabilities = self.firefox_capabilities()
        elif browser == 'chrome-headless':
            capabilities = self.chrome_headless_capabilities()
        elif browser == 'chrome':
            capabilities = self.chrome_capabilities()
        logging.info(f"Setting capabilities for {browser}...")
        return capabilities

    def set_command_executor(self):
        browser = self.BROWSER
        execution_type = self.EXECUTION_TYPE
        command_executor = None
        if execution_type == 'grid':
            command_executor = 'http://127.0.0.1:4444/wd/hub'
        elif execution_type == 'gitlabci' and browser == 'chrome' or browser == 'chrome-headless':
            command_executor = 'http://selenium__standalone-chrome:4444/wd/hub'
        elif execution_type == 'gitlabci' and browser == 'firefox':
            command_executor = 'http://selenium__standalone-firefox:4444/wd/hub'
        logging.info(f"Getting command executor for {browser}...")
        logging.info(f"Tests will be run in {command_executor}")
        return command_executor

    def select_browser(self):
        browser = self.BROWSER
        execution_type = self.EXECUTION_TYPE
        driver = None
        if execution_type == 'local':
            if browser == 'firefox':
                driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
            elif browser == 'chrome':
                driver = webdriver.Chrome(ChromeDriverManager().install())
            elif browser == 'chrome-headless':
                driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(),
                                          desired_capabilities=self.chrome_headless_capabilities())
            elif browser == 'ie':
                driver = webdriver.Ie(IEDriverManager().install())
            else:
                raise ValueError(
                    f'--browser="{browser}" is not defined in conftest.py')

        elif execution_type == 'grid':
            driver = webdriver.Remote(command_executor=self.set_command_executor(),
                                      desired_capabilities=self.set_capabilities())

        elif execution_type == 'gitlabci':
            driver = webdriver.Remote(command_executor= self.set_command_executor(),
                                      desired_capabilities=self.set_capabilities())

        logging.info(f"Setting {execution_type} driver with {browser}...")

        return driver